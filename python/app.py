from jnpr.junos import Device
from pprint import pprint
import json

inventory = [
        "dallas-fw0",
        "houston-fw0",
        "austin-fw0"
    ]

list_of_device_output = []
for each_hostname in inventory:
    with Device(host=each_hostname, user='automation', password='juniper123') as network_device:
        try:
            show_version = network_device.rpc.get_software_information({'format':'json'})
            payload = {'hostname': each_hostname, 'payload': show_version}
            list_of_device_output.append(payload)
            pprint('Completed request on device ' + str(each_hostname))
        except:
            pass

pprint(list_of_device_output)